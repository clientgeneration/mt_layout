module MtLayout
  module Generators
    class InstallSidemenuGenerator < Rails::Generators::Base
        # this line defines the source path, defaults to parent app
        source_root File.expand_path("../../templates", __FILE__)

        def copy_layout_file
            copy_file '../../../app/views/layouts/mt_layout/sidemenu.html.erb', 'app/views/layouts/sidemenu.html.erb'
        end

        def copy_layout_partials
            copy_file '../../../app/views/layouts/mt_layout/shared/sidemenu/_sidenav.html.erb', 'app/views/layouts/mt_layout/shared/sidemenu/_sidenav.html.erb'
            copy_file '../../../app/views/layouts/mt_layout/shared/sidemenu/_topnav.html.erb', 'app/views/layouts/mt_layout/shared/sidemenu/_topnav.html.erb'
        end

        def copy_stylesheets
            if yes? ("Copy sidemenu css assets? no means reference")
                copy_file '../../../app/assets/stylesheets/mt_layout/sidemenu/basic.css.scss', 'app/assets/stylesheets/mt_layout/sidemenu/basic.css.scss'
                copy_file '../../../app/assets/stylesheets/mt_layout/sidemenu/sidemenu.css.scss', 'app/assets/stylesheets/mt_layout/sidemenu/sidemenu.css.scss'
                copy_file '../../../app/assets/stylesheets/mt_layout/sidemenu/topnav.css.scss', 'app/assets/stylesheets/mt_layout/sidemenu/topnav.css.scss'
            else
                insert_into_file "app/assets/stylesheets/application.css", :after => "*= require_tree ." do
                    "\n *= require mt_layout/sidemenu/basic.css.scss" +
                    "\n *= require mt_layout/sidemenu/sidemenu.css.scss" +
                    "\n *= require mt_layout/sidemenu/topnav.css.scss"
                end
            end
        end

        def copy_javascripts
            if yes? ("Copy sidemenu js assets? no means reference")
                copy_file '../../../app/assets/javascripts/mt_layout/sidemenu/sidemenu.js', 'app/assets/javascripts/mt_layout/sidemenu/sidemenu.js'
            else
                insert_into_file "app/assets/javascripts/application.js", :after => "//= require_tree ." do
                    "\n//= require mt_layout/sidemenu/sidemenu.js"
                end
            end
        end

        def copy_ripple_buttons
            #  must be present for sidemenu layout
            if yes?("Copy ripple buttons assets? no means reference")
                copy_file '../../../app/assets/stylesheets/mt_layout/shared/button_ripple.css.scss', 'app/assets/stylesheets/mt_layout/shared/button_ripple.css.scss'
                copy_file '../../../app/assets/javascripts/mt_layout/shared/button_ripple.js', 'app/assets/javascripts/mt_layout/shared/button_ripple.js'
            else
                insert_into_file "app/assets/javascripts/application.js", :after => "//= require_tree ." do
                    "\n//= require mt_layout/shared/button_ripple.js"
                end
                insert_into_file "app/assets/stylesheets/application.css", :after => "*= require_tree ." do
                    "\n *= require mt_layout/shared/button_ripple.css.scss"
                end
            end
            copy_file '../../../app/views/mt_layout/shared/_ripple_circle.html.erb', 'app/views/mt_layout/shared/_ripple_circle.html.erb'
        end

        def copy_core_assets
            copy_file '../../../app/assets/stylesheets/mt_layout/core/variables.css.scss', 'app/assets/stylesheets/mt_layout/core/variables.css.scss'
        end

        def copy_standard_components
            if yes?("Do you want modern components?")
                if yes? ("Copy? no means reference")
                    copy_file '../../../app/assets/stylesheets/mt_layout/shared/components.css.scss', 'app/assets/stylesheets/mt_layout/shared/components.css.scss'
                    copy_file '../../../app/assets/stylesheets/mt_layout/shared/styles.css.scss', 'app/assets/stylesheets/mt_layout/shared/styles.css.scss'
                else
                    insert_into_file "app/assets/stylesheets/application.css", :after => "*= require_tree ." do
                        "\n *= require mt_layout/shared/styles.css.scss" + "\n *= require mt_layout/shared/components.css.scss"
                    end
                end
            end
        end

        def notice
            puts "\n============================================================"
            puts "After install configurations"
            puts "============================================================"
            puts "1. Make sure you have gravatar_top_nav defined in user decorator"
            puts "2. Remove mt_layout/application stylesheets and javascript from the layout file"
            puts "3. Add this $grid-float-breakpoint: 0px; to your bootstrap_init file to prevent navbar from collapsing\n\n"
        end
    end
  end
end