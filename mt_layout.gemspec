$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "mt_layout/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "mt_layout"
  s.version     = MtLayout::VERSION
  s.authors     = ["Hong Lance Lui"]
  s.email       = ["honglui@multiplier.io"]
  s.homepage    = "http://multiplier.io"
  s.summary     = "Provides layout to application."
  s.description = "Designed to make creating application layout fast and easy."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", ">= 4.1"

  s.add_development_dependency "sqlite3"
end
