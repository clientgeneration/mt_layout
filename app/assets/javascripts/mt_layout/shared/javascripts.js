var onCanvasMenuInit = function(){
	$("body").on("click", ".on-canvas-menu-btn", function(){
		id = $(this).data('id')
		onCanvas = $("body").find(".on-canvas[data-id="+id+"]") 
		onCanvas.show();
		onCanvas.find(".on-canvas-menu").show("slide", {direction: "left"}, 300);
	});

	$("body").on("click", ".on-canvas-overlay", function(){
		id = $(this).data('id')
		onCanvas = $("body").find(".on-canvas[data-id="+id+"]") 
		onCanvas.find(".on-canvas-menu").hide("slide", {direction: "left"}, 300, function(){
			onCanvas.hide();
		});
	});
};

$(document).ready(onCanvasMenuInit);
$(document).on('page:load', onCanvasMenuInit);

var hideOnCanvas = function (id){
	onCanvas = $("body").find(".on-canvas[data-id="+id+"]") 
	onCanvas.find(".on-canvas-menu").hide("slide", {direction: "left"}, 300, function(){
		onCanvas.hide();
	});
}