// Ripple button must have container that has position relative
// Insert the following lines into ripple button container
// <%= render 'mt_layout/shared/ripple_circle' %>

// Credits: http://codepen.io/KrisOlszewski/pen/Dnktj

function buttonRippleInit() {
  // 'use strict';

  var $ripples = $('.ripples');

  $ripples.on('click.Ripples', function(e) {

    var $this = $(this);
    var $offset = $this.parent().offset();
    var $circle = $this.find('.ripples__circle');

    var x = e.pageX - $offset.left;
    var y = e.pageY - $offset.top;

    $circle.css({
      top: y + 'px',
      left: x + 'px'
    });

    $this.addClass('is-active');

  });

  $ripples.on('animationend webkitAnimationEnd oanimationend MSAnimationEnd', function(e) {
  	$(this).removeClass('is-active');
  });
}

$(document).ready(buttonRippleInit);
$(document).on('page:load', buttonRippleInit);