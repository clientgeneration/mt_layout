function sidemenuInit() {
	// control toggling sidemenu
	$( "#sidemenu-toggle-btn" ).on( "click", toggleSidemenu );
	function toggleSidemenu(){
		var button = $(this);

		// different visibility for mobile and desktop devices
		if ($( window ).width()>=992){
			// close sidemenu
			if ($("#sidemenu-layout").hasClass("sidemenu-visible-md")){
				$("#sidemenu-layout").removeClass("sidemenu-visible-xs");
				$("#sidemenu-layout").removeClass("sidemenu-visible-md");
			}else{
				$("#sidemenu-layout").addClass("sidemenu-visible-md");
			}
		}else{
			// close sidemenu
			if ($("#sidemenu-layout").hasClass("sidemenu-visible-xs")){
				$("#sidemenu-layout").removeClass("sidemenu-visible-xs");
			}else{
				$("#sidemenu-layout").addClass("sidemenu-visible-xs");
			}
		}
	};

	// control dropdown items in sidemenu
	$( ".sidemenu-item-dropdown" ).on( "click", toggleSidemenuDropdown );
	function toggleSidemenuDropdown(){
		if ($(this).closest("li").hasClass("dropdown-close")){
			$(this).find('.sidemenu-dropdown-icon').css({'-webkit-transform' : 'rotate(-90deg)',
				'-moz-transform' : 'rotate(-90deg)',
				'-ms-transform' : 'rotate(-90deg)',
				'transform' : 'rotate(-90deg)'});
			
			$(this).next('.sidemenu-subnav').show( "blind", { direction: "up" }, 200);
			$(this).closest("li").removeClass("dropdown-close");
			$(this).closest("li").addClass("dropdown-open");
		}else{
			$(this).find('.sidemenu-dropdown-icon').css({'-webkit-transform' : 'rotate(0deg)',
				'-moz-transform' : 'rotate(0deg)',
				'-ms-transform' : 'rotate(0deg)',
				'transform' : 'rotate(0deg)'});
			
			$(this).next('.sidemenu-subnav').hide( "blind", { direction: "up" }, 200);
			$(this).closest("li").addClass("dropdown-close");
			$(this).closest("li").removeClass("dropdown-open");
		}
	};

};

$(document).ready(sidemenuInit);
$(document).on('page:load', sidemenuInit);