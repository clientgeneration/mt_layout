require_dependency "mt_layout/application_controller"

module MtLayout
	class PagesController < ApplicationController
		def sidemenu
			render layout: 'mt_layout/sidemenu'
		end

		def topmenu
			render layout: 'mt_layout/topmenu'
		end
	end
end
